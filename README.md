# Greek and English LuaLaTeX Hyphenation

A Fedora Docker image with LuaLaTeX and both Greek and English hyphenation rules
enabled.

If you want to use it and work in the current directory, execute

```
docker run -it -v "$PWD":/data registry.gitlab.com/pothitos/greek-english-hyphenation
```

Then, you should `cd /data` in order to access the current working directory
from inside the Docker container.
