FROM registry.gitlab.com/pothitos/texlive-full:35.48

# Concatenate English and Greek hyphenation rules and install them
RUN cat \
    /usr/share/texlive/texmf-dist/tex/generic/hyph-utf8/patterns/tex/hyph-el-monoton.tex \
    >> /usr/share/texlive/texmf-dist/tex/generic/hyphen/hyphen.tex && \
    mktexfmt lualatex.fmt
